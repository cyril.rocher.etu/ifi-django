from accueil import views
from django.urls import path, re_path , include

urlpatterns = [
    path('depreciatedAccueil', views.deprecatedAccueil),
    re_path(r'^$', views.goodAccueil),
    path('sum/<int:first>/<int:second>', views.sum),
    re_path(r'^sum/(?P<first>)/(?P<second>)', views.sum),
]