from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

# Create your views here.
def deprecatedAccueil(request):
    """ Exemple de page non valide au niveau HTML pour que l'exemple soit concis """
    return HttpResponse("""
        <h1>Hello world</h1>
    """)

def goodAccueil(request):
    return render(request, 'accueil/helloWorld.html', {'date': datetime.now()})

def sum(request,first, second):
    total=first+second
    return render(request,'accueil/sum.html',locals())