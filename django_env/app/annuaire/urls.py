from annuaire import views
from django.urls import path, re_path , include

app_name = 'annuaire'
urlpatterns = [
    re_path(r'^$', views.getList, name='list'),
    path('particulier/<int:id>', views.getParticulier, name= 'particulier'),
    path('<couleur>', views.getList, name='list'),
    path('entreprise/<int:id>', views.getEntreprise, name= 'entreprise'),

    #re_path(r'particulier/(P<id>[0-9]+)$', views.getParticulier),
    ]