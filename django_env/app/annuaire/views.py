from django.shortcuts import render
from django.http import HttpResponse
from .models import Particulier
# Create your views here.

def getList(request, couleur="blanches"):
    if(couleur == "blanches"):
        contacts = [
            {'id': 0, 'nom': 'Nom', 'prenom': 'Prénom', 'telephone':'03 12 34 66 55', "entreprise" : False},
            {'id': 1, 'nom': 'Tata', 'prenom': 'Toto', 'telephone':'03 99 99 99 99', "entreprise" : False},
            {'id': 2, 'nom': 'Lechien', 'prenom': 'Bobby', 'telephone':'03 01 01 01 01', "entreprise" : False},
        ]
    else : 
        contacts = [
            {'id': 0, 'nom' : 'EDF', "prenom" : "", 'telephone':'000000000', "entreprise" : True},
            {'id': 1, 'nom' : 'GDF', "prenom" : "", 'telephone':'030000000', "entreprise" : True},
        ]
    return render(request, 'annuaire/list.html',{'contacts': contacts , 'couleur' : couleur})

def getParticulier(request, id):
    particulier = Particulier.objects.get(pk=id)
    return render(request, 'annuaire/detail.html', {'particulier': particulier})

def getParticulier(request, id):
    particuliers = [
        {'id': 0, 'nom': 'Nom', 'prenom': 'Prénom', 'telephone':'03 12 34 66 55', "entreprise" : False},
        {'id': 1, 'nom': 'Tata', 'prenom': 'Toto', 'telephone':'03 99 99 99 99', "entreprise" : False},
        {'id': 2, 'nom': 'Lechien', 'prenom': 'Bobby', 'telephone':'03 01 01 01 01', "entreprise" : False},
    ]
    return render(request, 'annuaire/detail.html', {'contact': particuliers[id]}) 

def getEntreprise(request, id):
    entreprises = [
            {'id': 0, 'nom' : 'EDF', "prenom" : "", 'telephone':'000000000', "entreprise" : True},
            {'id': 1, 'nom' : 'GDF', "prenom" : "", 'telephone':'030000000', "entreprise" : True},
    ]
    return render(request, "annuaire/detail.html", {"contact": entreprises[id]})