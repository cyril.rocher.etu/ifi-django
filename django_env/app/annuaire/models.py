from django.db import models

class Particulier(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    telephone = models.CharField(max_length=14)
    entreprise = models.BooleanField(default=False)

    def __str__(self):
        return self.nom + ' ' + self.prenom

    

