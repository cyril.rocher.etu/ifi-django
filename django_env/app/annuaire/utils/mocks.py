class Particuliers():

    PARTICULIERS = [
        {'id': 0, 'Nom': 'Nom1', 'Prénom': 'Prénom1', 'Téléphone':'06 12 34 66 55'},
        {'id': 1, 'Nom': 'Nom2', 'Prénom': 'Florian', 'Téléphone':'06 99 99 99 99'},
        {'id': 2, 'Nom': 'Lechien', 'Prénom': 'Bobby', 'Téléphone':'06 01 01 01 01'},
    ]

    def all(self):
        return self.PARTICULIERS

    def find(self, id):
        return self.PARTICULIERS[int(id)]
