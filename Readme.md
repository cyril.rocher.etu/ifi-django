**Migration**

- Pour montrer les migrations en attentes
`./manage.py showmigrations`

- Pour générer toutes les migrations nécessaires pour l'application
`./manage.py makemigrations`

- Pour faire la migration
`./manage.py migrate`

- Pour lancer le shell python
`./managde.py shell`  

##### Commandes utiles

_Créer un particulier par étape_  
**>>> from annuaire.models import Particulier**  
**>>> Particulier**  
**>>> particulier = Particulier()**  
**>>> particulier.nom = 'DELAVALLE'**  
**>>> particulier.prenom= 'Rémi'**  
**>>> particulier.telephone = '06 11 11 11 11'**  
**>>> particulier.save()**  

_Créer un particulier en une ligne (il faut avoir importé le model)_  
**>>> particulier = Particulier(nom='ROCHER', prenom='Cyril',telephone='06 12 12 12 12')**  
**>>> particulier.save()**  

_Supprimer la ligne bdd_  
**>>> particulier.delete()**  
_________________________
**>>> particulier = Particulier(nom='ROCHER', prenom='Cyril',telephone='06 12 12 12 12')**  

_Créer un particulier via la méthode create de l'objet_  
**>>> Particulier.objects.create(nom='BAZIR', prenom='Allyson',telephone='06 13 13 13 13')**  

_Voir tout les objets créé dans la base particulier_  
**>>> Particulier.objects.all()**  

_Afficher une valeur de chaque instance créée_  
**>>> for particulier in Particulier.objects.all():**  
**>>> print(particulier.nom)**  

_Afficher tout les objets séléctionnés par un filtre_  
**>>> for particulier in Particulier.objects.filter(nom="DELAVALLE"):**  
**>>> print(particulier.titre, "par", particulier.auteur)**  

_Trier les objets créés par un champ spécifique_  
**>>> Particulier.objects.order_by('telephone')**  

_Trier dans l'ordre inverse_  
**>>> Particulier.objects.order_by('-telephone')**  

_Get avec une condition_  
**>>> Particulier.objects.get(telephone__startswith="06")**  

_Get et créer si l'instance n'existe pas__  
**>>> Article.objects.get_or_create(nom="Fabien", prenom='delbecque', telephone="06 14 14 14 14")**  

_Sortir_  
**>>> exit()**  

./manage.py makemigrations  

You are trying to add a non-nullable field 'entreprise' to particulier without a default; we can't do that (the database needs something to populate existing rows).  
Please select a fix:  
 1) Provide a one-off default now (will be set on all existing rows with a null value for this column)  
 2) Quit, and let me add a default in models.py  
Select an option: 2  

**>>> 2**  

## Ajouter default=False dans le modèle  

**>>> ./manage.py makemigrations**  

Migrations for 'annuaire':  
  annuaire/migrations/0002_particulier_entreprise.py  
    - Add field entreprise to particulier  

**>>> ./manage.py migrate**  

Operations to perform:  
  Apply all migrations: admin, annuaire, auth, contenttypes, sessions  
Running migrations:  
  Applying annuaire.0002_particulier_entreprise... OK  

**>>> Particulier.objects.create(nom='Université de Lille', prenom='',telephone='06 44 44 44 44', entreprise=True)**

**>>> Particulier.objects.get_or_create(nom='Hopital Margoulin', prenom='',telephone='06 45 45 45 45', entreprise=True)**


